import { useState } from "react";
// import './components/Count.jsx'
import Count from "./components/Board";
function App() {
  const colAndRow = {
    row1: [],
    row2: [],
    row3: [],

    col1: [],
    col2: [],
    col3: [],

    dia1: [],
    dia2: [],
    selected: 0,
  };
  let [winner, setWinner] = useState("");

  return (
    <>
      {(winner !== "" && <h1>Winner:{winner}</h1>) || (
        <Count colAndRow={colAndRow} setWinner={setWinner} />
      )}
    </>
  );
}

export default App;
