import React, { useState } from "react";

function Box({ val, valfun, row, col, colAndRow, setWinner }) {
  function checkWinner(array) {
    if (array[0] === array[1] && array[1] === array[2]) {
      setWinner(array[0]);
      return true;
    } else {
      return false;
    }
  }

  let [value, setValue] = useState("");
  let rowSelected =
    (row == 1 && colAndRow.row1) ||
    (row == 2 && colAndRow.row2) ||
    (row == 3 && colAndRow.row3);
  let colSelected =
    (col == 1 && colAndRow.col1) ||
    (col == 2 && colAndRow.col2) ||
    (col == 3 && colAndRow.col3);

  let dia1Selected = col == row && colAndRow.dia1;
  let dia2Selected =
    (col == 3 && row == 1 && colAndRow.dia2) ||
    (col == 2 && row == 2 && colAndRow.dia2) ||
    (col == 1 && row == 3 && colAndRow.dia2);

  const onClickFunction = () => {
    if (value === "") {
      val = (val == "" && val == "O") || (val == "O" && "X") || "O";
      colAndRow.selected += 1;
      if (colAndRow.selected == 9) {
        setWinner("draw");
      }

      valfun(val);
      setValue(val);
      rowSelected.push(val);
      colSelected.push(val);
      if (dia1Selected) {
        dia1Selected.push(val);
      }
      if (dia2Selected) {
        dia2Selected.push(val);
      }

      const checkedWinner =
        (rowSelected.length == 3 && checkWinner(rowSelected)) ||
        (colSelected.length == 3 && checkWinner(colSelected)) ||
        (dia1Selected.length == 3 && checkWinner(dia1Selected)) ||
        (dia2Selected.length == 3 && checkWinner(dia2Selected));
    }
  };
  return (
    <div className="box" onClick={onClickFunction}>
      <h1>{value}</h1>
    </div>
  );
}

export default Box;
