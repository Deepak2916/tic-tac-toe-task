import React, { useState } from "react";
import "./Board.css";
import Box from "./Box";

function Board({ colAndRow, setWinner }) {
  let [value, setValue] = useState("");

  return (
    <>
      <div className="main-container">
        {[1, 2, 3].map((row) => {
          return (
            <div key={row}>
              {[1, 2, 3].map((col) => {
                return (
                  <Box
                    key={`${row}${col}`}
                    row={row}
                    col={col}
                    val={value}
                    valfun={setValue}
                    colAndRow={colAndRow}
                    setWinner={setWinner}
                  ></Box>
                );
              })}
            </div>
          );
        })}
      </div>
    </>
  );
}

export default Board;
